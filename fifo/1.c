#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

int main()
{
	int fd;
	char *myfifo = "/tmp/myfifo";
	
	mkfifo(myfifo, 0666);

	fd = open(myfifo, O_WRONLY);
	write(fd, "Hi", sizeof("Hi"));
	close(fd);

	unlink(myfifo);

	return 0;
}
