#include <stdio.h>
#include <unistd.h>

int main()
{
	pid_t pid;

	printf("Jestem sam\n");
	printf("Wolam fork");

	pid = fork();
	if(pid == 0)
		printf("jestem potomkiem\n");
	else if(pid > 0)
		{ printf("Jestem rodzicem, nowy proces to PID=%d\n", pid);
		wait();}
	else
		printf("Fork zwocil blad - brak potomka\n");
	return 0;
}
