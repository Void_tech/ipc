#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>

MODULE_LICENSE("Dual BSD/GPL");

static int __init hello_init(void)
{
	printk(KERN_ALERT "Hello kernel with process=%s (PID=%i, parent= %s)\n",
	current->comm, current->pid, current->parent->comm);
	return 0;
}

static void __exit hello_exit(void)
{
	printk(KERN_ALERT "Goodbey kernel world.\n");
}

module_init(hello_init);
module_exit(hello_exit);
